# ubuntu-mate-icon-themes

The official icon themes for Ubuntu MATE

https://launchpad.net/ubuntu-mate/

Files URL:

http://mirrors.kernel.org/ubuntu/pool/universe/u/ubuntu-mate-artwork/

https://mirrors.edge.kernel.org/ubuntu/pool/universe/u/ubuntu-mate-artwork/

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/themes-and-icons/ubuntu-mate-icon-themes.git
```

